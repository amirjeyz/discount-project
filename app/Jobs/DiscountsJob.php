<?php

namespace App\Jobs;

use App\Models\Discount;
use App\Models\DiscountCode;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class DiscountsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $discount;
    protected $createNumber;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Discount $discount, $createNumber)
    {
        $this->discount = $discount;
        $this->createNumber = $createNumber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 172800);

        DB::beginTransaction();
        try {
            $codeArray = [];

            for ($i = 0; $i < $this->createNumber; $i++) {
                $codeArray[] = [
                    'code' => DiscountCode::generateCode($this->discount->id),
                    'discount_id' => $this->discount->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
            $codeArray = collect($codeArray);
            $chunks = $codeArray->chunk(500)->toArray();

            DB::disableQueryLog();
            foreach ($chunks as $chunk) {
                DiscountCode::insert($chunk);
            }
            $this->discount->update([
                'is_ready' => 1
            ]);
            DB::commit();
            Log::info('Discount successfully added.');
        } catch (ValidationException $e) {
            DB::rollback();
            Log::info("Discount has Error \n" . $e);
        }
    }

    /**
     * The job failed to process.
     *
     * @param \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        Log::info($exception);
    }
}
