<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'price',
        'expire_time'
    ];

    public function getDiscounts()
    {
        return $this->discounts();

    }


    // Relations
    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_package', 'package_id', 'order_id');
    }

    public function discounts()
    {
        return $this->belongsToMany(Discount::class, 'discount_package', 'package_id', 'discount_id');
    }

}
