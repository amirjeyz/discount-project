<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'start_time',
        'end_time',
        'first_purchase',
        'usable_count',
        'percent_off',
        'is_ready'
    ];

    public static function validateDiscountCode($code, $package)
    {
        $package = Package::whereId($package)->first();

        $discountIDs = [];
        foreach ($package->discounts as $key => $discounts) {
            $discountIDs[] = $discounts->pivot->discount_id;
        }

        $checkHasDiscountCode = DiscountCode::whereIn('discount_id', $discountIDs)
                                            ->whereCode($code)
                                            ->where('is_used', 0)
                                            ->first();

        if ( ! $checkHasDiscountCode) {
            return false;
        }

        return true;

    }

    public static function calculateDiscount($package, $code)
    {
        $discountIDs = [];
        foreach ($package->discounts as $key => $discounts) {
            $discountIDs[] = $discounts->pivot->discount_id;
        }

        $getCode = DiscountCode::whereIn('discount_id', $discountIDs)
                            ->whereCode($code)
                            ->first();

        $percent = $getCode->discount->percent_off;

        $totalPrice = ( $package->price * $percent ) / 100;

        return $totalPrice;
    }

    public function discountDiffTime()
    {
        $now  = Carbon::now();
        $end  = $this->end_time;
        $daysLeft = $now->diffInDays($end);

        return $daysLeft;
    }



    // Relations
    public function codes()
    {
        return $this->hasMany(DiscountCode::class);
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'discount_package', 'discount_id', 'package_id');
    }
}
