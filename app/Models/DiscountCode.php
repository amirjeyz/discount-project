<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DiscountCode extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'discount_id',
        'is_used'
    ];

    public static function generateCode($id): string
    {
        $lengthArray = [8, 9, 10];
        $length = $lengthArray[array_rand($lengthArray)] - 7;

        $number = (Carbon::now()->timestamp * $id * Carbon::now()->format('v'));
        $numberToMD5 = md5($number);

        $hashLength = strlen($numberToMD5);

        for ($i = 0; $i < 4; $i++) {
            $randomIndex = mt_rand(0, $hashLength - 1);
            $uniqueString = $numberToMD5[$randomIndex];
            $stringArray[] = $uniqueString;
        }
        $arrayToString = $stringArray[0] . $stringArray[3] . $stringArray[2] . $stringArray[1];

        if ( $lengthArray[array_rand($lengthArray)] == 8 ) {
            $code = $arrayToString . Str::random(3) . Str::random( $length );
        } elseif ( $lengthArray[array_rand($lengthArray)] == 9 ) {
            $code =  Str::random(3) . $arrayToString . Str::random( $length );
        } else {
            $code = Str::random( $length ) . $arrayToString . Str::random(3);
        }

        return $code;
    }

    // Relations
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }
}
