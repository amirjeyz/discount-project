<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\DiscountsJob;
use App\Models\Discount;
use App\Models\DiscountCode;
use App\Models\Package;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class DiscountsController extends Controller
{
    public function index()
    {
        $discounts = Discount::orderBy('id', 'DESC')
                            ->paginate(3);
        return view('admin.discount.index', compact('discounts'));
    }

    public function create()
    {
        return view('admin.discount.create', [
            'packages' => Package::all()
        ]);
    }

    public function store(Request $request)
    {
        $validData = $request->validate([
            'title' => 'required|unique:discounts,title',
            'start_time' => 'required',
            'end_time' => 'required',
            'usable_count' => 'required',
            'first_purchase' => 'required',
            'packages' => 'required',
            'create_number' => 'required',
        ]);
//        return DiscountCode::generateCode(3);
        $discount = Discount::create([
            'title' => $request->title,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'usable_count' => $request->usable_count,
            'first_purchase' => $request->first_purchase,
            'percent_off' => $request->percent_off,
        ]);

        $discount->packages()->attach($request->packages);

        DiscountsJob::dispatch($discount, $request->create_number);

        return back()->with('success', 'Discount successfully added. Please wait to create Codes.');
    }

    public function edit(Discount $discount)
    {
        return view('admin.discount.edit', compact('discount'));
    }

    public function update(Discount $discount, Request $request)
    {
        $validData = $request->validate([
            'start_time' => 'required',
            'end_time' => 'required',
            'usable_count' => 'required',
            'first_purchase' => 'required',
        ]);

        $discount->update([
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'first_purchase' => $request->first_purchase,
            'usable_count' => $request->usable_count,
        ]);

        return back()->with('success', 'Discount successfully updated.');
    }

    public function destroy(Discount $discount)
    {
        $discount->delete();
        return back()->with('success', 'Discount successfully deleted.');
    }

    public function singleDestroy($id)
    {
        DiscountCode::find($id)->delete();
        return back()->with('success', 'Discount successfully deleted.');
    }
}
