<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    public function index()
    {
        return view('admin.packages.index', [
            'packages' => Package::orderBy('id', 'DESC')->get()
        ]);
    }

    public function create()
    {
        return view('admin.packages.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'expire_time' => 'required'
        ]);

        $package = Package::create([
            'title' => $request->title,
            'description' => $request->description,
            'price' => $request->price ?? 0,
            'expire_time' => $request->expire_time
        ]);

        return back()->with('success', 'Package successfully added');
    }
}
