<?php

namespace App\Http\Controllers;

use App\Models\Discount;
use App\Models\Package;
use App\Models\DiscountCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    public function index()
    {
        return view('packages', [
            'packages' => Package::all()
        ]);
    }

    public function show(Package $package)
    {
        return view('single', [
            'package' => $package
        ]);
    }

    public function calculateTotalPrice(Request $request, Package $package)
    {
        $discountIDs = [];
        foreach ($package->discounts as $discount) {
            $discountIDs[] = $discount->pivot->discount_id;
        }

        if ( ! count($discountIDs) > 0 ) {
            return back()->with('error', 'This package has not discounts');
        }

        $discounts = Discount::whereIn('id', $discountIDs)->get();

        if (! $discount->usable_count > 0) {
            return back()->with('error', 'capacity of the discount is exhausted.');
        }

        /// Check user has paid order and discount has time
        foreach ($discounts as $discount) {
            if ($discount->discountDiffTime() == 0) {
                return back()->with('error', 'Discount time is finished.');
            }
            if ($discount->first_purchase == 1) {
                if ( Auth::user()->userHasPaidOrder() ) {
                    return back()->with('error', 'You cannot use discount!');
                }
            }
        }

        $validateDiscountCode = \App\Models\Discount::validateDiscountCode($request->code, $package->id);
        if (! $validateDiscountCode) {
            return back()->with('error', 'Code is wrong!');
        }

        $getTotalPrice = \App\Models\Discount::calculateDiscount($request->package, $request->code);

        if ($discount->usable_count == 1) {
            $discount->usable_count = 0;
            $discount->save();
        }

        DiscountCode::whereCode($request->code)
                    ->update([
                        'is_used' => 1
                    ]);

        return back()->with('total-price', $getTotalPrice . ' Rial');
    }

}
