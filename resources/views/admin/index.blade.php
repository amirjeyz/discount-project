@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <a href="{{route('admin.index')}}">Dashboard</a>
                    <a href="">Packages</a>
                    <a href="">Coupons</a>
                    <a href="">Users</a>
                </div>

                <div class="card-body">
                    <h1>Hey!</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
