@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">

                    <div class="card-header">Edit {{$discount->title}}</div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form action="{{route('admin.discounts.update', $discount)}}" method="POST">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label>Start Time:</label>
                                <input type="datetime-local" class="form-control" name="start_time" value="{{$discount->start_time}}">
                            </div>

                            <div class="form-group">
                                <label>End Time:</label>
                                <input type="datetime-local" class="form-control" name="end_time" value="{{$discount->end_time}}">
                            </div>

                            <div class="form-group">
                                <label>Usable Count:</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="usable_count" id="exampleRadios1" value="1" {{$discount->usable_count == 1 ? 'checked' : ''}}>
                                    <label class="form-check-label" for="exampleRadios1">
                                        One Person
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="usable_count" id="exampleRadios1" value="99999999999999" {{$discount->usable_count == 99999999999999 ? 'checked' : ''}}>
                                    <label class="form-check-label" for="exampleRadios1">
                                        Many Person
                                    </label>
                                </div>
                            </div>


                            <div class="form-group">
                                <label>First Purchase:</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="first_purchase" id="exampleRadios1" value="1" {{$discount->first_purchase == 1 ? 'checked' : ''}}>
                                    <label class="form-check-label" for="exampleRadios1">
                                        True
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="first_purchase" id="exampleRadios1" value="0" {{$discount->first_purchase == 0 ? 'checked' : ''}}>
                                    <label class="form-check-label" for="exampleRadios1">
                                        False
                                    </label>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
