@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="d-flex justify-content-between mb-5">
                    <h3>Discounts list</h3>
                    <a href="{{route('admin.discounts.create')}}" class="btn btn-primary">+ Create new</a>
                </div>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Codes</th>
                            <th>Usable Counts</th>
                            <th>First Purchase</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($discounts as $discount)
                            <tr>
                                <td>{{$discount->id}}</td>
                                <td>{{$discount->title}}</td>
                                <td>{{$discount->start_time}}</td>
                                <td>{{$discount->end_time}}</td>
                                <td>
                                    @if (! $discount->is_ready)
                                        <span class="badge badge-sm badge-warning">is loading...</span>
                                    @else
                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal{{$discount->id}}">
                                            {{count($discount->codes)}} Codes
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal{{$discount->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Codes</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @foreach($discount->codes as $code)
                                                            <ul class="list-group">
                                                                <li class="list-group-item">
                                                                    <div class="d-flex justify-content-between">
                                                                        <span>Code: {{$code->code}}</span>
                                                                        <form action="{{route('admin.discounts.single.delete', $code->id)}}" method="post">
                                                                            @csrf
                                                                            @method('DELETE')
                                                                            <button class="btn btn-danger btn-sm ml-1" type="submit">Delete</button>
                                                                        </form>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        @endforeach
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </td>
                                <td>{{$discount->usable_counts == 1 ? '1 Person' : 'Many Person' }}</td>
                                <td>{{$discount->first_purchase ? 'True' : 'False'}}</td>
                                <td>{{$discount->created_at}}</td>
                                <td class="d-flex justify-content-start">
                                    <a href="{{route('admin.discounts.edit', $discount)}}" class="btn btn-sm btn-primary">Edit</a>
                                    <form action="{{route('admin.discounts.delete', $discount)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm ml-1" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
