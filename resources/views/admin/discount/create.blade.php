@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">

                    <div class="card-header">Create Discount</div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        <form action="{{route('admin.discounts.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Title:</label>
                                <input type="text" class="form-control" name="title" placeholder="Say Smthng" value="{{old('title')}}">
                            </div>

                            <div class="form-group">
                                <label>Start Time:</label>
                                <input type="datetime-local" class="form-control" name="start_time" value="{{old('start_time')}}">
                            </div>

                            <div class="form-group">
                                <label>End Time:</label>
                                <input type="datetime-local" class="form-control" name="end_time" value="{{old('end_time')}}">
                            </div>

                            <div class="form-group">
                                <label>Usable Count:</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="usable_count" id="exampleRadios1" value="1">
                                    <label class="form-check-label" for="exampleRadios1">
                                        One Person
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="usable_count" id="exampleRadios1" value="99999999999999">
                                    <label class="form-check-label" for="exampleRadios1">
                                        Many Person
                                    </label>
                                </div>
                            </div>


                            <div class="form-group">
                                <label>First Purchase:</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="first_purchase" id="exampleRadios1" value="1">
                                    <label class="form-check-label" for="exampleRadios1">
                                        True
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="first_purchase" id="exampleRadios1" value="0">
                                    <label class="form-check-label" for="exampleRadios1">
                                        False
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Package:</label>
                                <select multiple class="form-control" name="packages[]">
                                    @foreach($packages as $package)
                                        <option value="{{$package->id}}">{{$package->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Number of discount code:</label>
                                <input type="number" class="form-control" name="create_number" value="{{old('create_number')}}">
                            </div>

                            <div class="form-group">
                                <label>Percent off:</label>
                                <input type="number" class="form-control" name="percent_off" value="{{old('percent_off')}}">
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
