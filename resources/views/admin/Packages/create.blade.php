@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">

                    <div class="card-header">Create package</div>

                    <div class="card-body">
                        <form action="" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Title:</label>
                                <input type="text" class="form-control" name="title" placeholder="Say Smthng">
                            </div>
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea name="description" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Price:</label>
                                <br>
                                <small class="text-danger">if you want to make this package free, leave the field blank </small>
                                <input type="number" class="form-control" name="price" placeholder="1000">
                            </div>
                            <div class="form-group">
                                <label>Expire Time:</label>
                                <select class="form-control" name="expire_time">
                                    <option disabled selected>Choose your expire time...</option>
                                    <option value="1">1 Month</option>
                                    <option value="3">3 Month</option>
                                    <option value="6">6 Month</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
