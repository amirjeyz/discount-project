@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="d-flex justify-content-between">
                    <h1>Packages List</h1>
                    <a href="" class="btn h-100 btn-primary">Create</a>
                </div>
                <hr>

                @foreach($packages as $package)
                    <div>
                        <h2> {{$package->title}}
                            <small style="font-size: 20px;">( {{$package->price}} Toman )</small>
                        </h2>
                        <p>{{$package->description}}</p>
                        <hr>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
