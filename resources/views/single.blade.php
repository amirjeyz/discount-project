@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Package') }}</div>

                <div class="card-body">
                    <h1>{{$package->title}}</h1>
                    <h5>{{$package->price}} Rial</h5>
                    <p>{{$package->description}}</p>

                    <form action="{{route('package.total', $package)}}" method="POST">
                        @csrf
                        <div class="form-group w-50">
                            <label for="">Discount:</label>
                            <input type="text" name="code" class="form-control">
                            <button type="submit" class="btn btn-sm btn-primary mt-1">Calculate</button>
                        </div>
                    </form>

                    @if (session('total-price'))
                        <div class="alert alert-success">
                            Total Price: {{ session('total-price') }}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
