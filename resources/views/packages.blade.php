@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($packages as $package)
                <div>
                    <a href="{{route('package.single', $package->id)}}">
                        <h1>{{$package->title}}</h1>
                    </a>
                    <h5>{{$package->price}} Rial</h5>
                    <p>{{$package->description}}</p>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
