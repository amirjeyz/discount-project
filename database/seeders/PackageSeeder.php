<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();


        Package::create([
            'title' => $faker->text(20),
            'description' => $faker->text(255),
            'price' => $faker->numberBetween(10000, 120000),
            'expire_time' => $faker->randomElement([1, 3, 6])
        ]);
    }
}
