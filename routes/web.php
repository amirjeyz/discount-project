<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

\Illuminate\Support\Facades\Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('package', [\App\Http\Controllers\PackageController::class, 'index'])->name('package');
Route::get('package/{package}', [\App\Http\Controllers\PackageController::class, 'show'])->name('package.single');
Route::post('package/{package}/discount', [\App\Http\Controllers\PackageController::class, 'calculateTotalPrice'])->name('package.total');



Route::name('admin.')->middleware(['auth', 'admin.auth'])->prefix('admin')->group(function () {
    Route::get('/', [\App\Http\Controllers\Admin\IndexController::class, 'index'])->name('index');

    Route::get('/packages', [\App\Http\Controllers\Admin\PackagesController::class, 'index'])->name('packages.index');
    Route::get('/packages/create', [\App\Http\Controllers\Admin\PackagesController::class, 'create'])->name('packages.create');
    Route::post('/packages/create', [\App\Http\Controllers\Admin\PackagesController::class, 'store'])->name('packages.store');


    Route::get('/discounts', [\App\Http\Controllers\Admin\DiscountsController::class, 'index'])->name('discounts.index');
    Route::get('/discounts/create', [\App\Http\Controllers\Admin\DiscountsController::class, 'create'])->name('discounts.create');
    Route::post('/discounts/create', [\App\Http\Controllers\Admin\DiscountsController::class, 'store'])->name('discounts.store');
    Route::get('/discounts/{discount}/edit', [\App\Http\Controllers\Admin\DiscountsController::class, 'edit'])->name('discounts.edit');
    Route::put('/discounts/{discount}/edit', [\App\Http\Controllers\Admin\DiscountsController::class, 'update'])->name('discounts.update');
    Route::delete('/discounts/{discount}/delete', [\App\Http\Controllers\Admin\DiscountsController::class, 'destroy'])->name('discounts.delete');
    Route::delete('/discounts/single/{id}/delete', [\App\Http\Controllers\Admin\DiscountsController::class, 'singleDestroy'])->name('discounts.single.delete');
});


Route::get('test', function () {
    return \App\Models\DiscountCode::generateCode();
});
